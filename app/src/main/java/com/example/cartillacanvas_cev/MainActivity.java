package com.example.cartillacanvas_cev;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.res.ResourcesCompat;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;


import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //Permisos de almacenamiento en el dispositivo
    private static void verifyStoragePermissions(Activity activity) {

        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    //onClick
    Button btnDibujarCartilla;
    Button btnGeneraPDF;

    //-------------------------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //llamada a pantalla para desplegar Cartilla
        btnDibujarCartilla = (Button) findViewById(R.id.llamaCartilla);
        btnDibujarCartilla.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("Boton", "Entro al evento onClick.");
                dibujarCartilla();
            }
        });

        //llamada a pantalla para desplegar Cartilla
        btnGeneraPDF = (Button) findViewById(R.id.llamaGeneraPdf);
        btnGeneraPDF.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Log.d("Boton", "Entro al evento que Generea un PDF");
                //exportaPDFCartilla();
                exportaTestPDF();
            }
        });
    }


    //----------------------------------------------------------------------------------------------------------
    //                                    GENERA IMAGEN CARTILLA
    //----------------------------------------------------------------------------------------------------------
    //METODO PRINCIPAL, INVOCA A TRAVES DEL EVENTO ONCLICK EL RETORNO DE IMAGEN CON EXTENSION .jpg
    public void dibujarCartilla() {
        Log.d("Cartilla", "Entro al metodo genera imagen Cartilla.");

        Bitmap bitmap = Bitmap.createBitmap(3507, 2480, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);

        canvas.drawRGB(244, 244, 244);

        //_______________________________________________________________________________________

        String nombreVacunado = "María Luisa Ortiz Gonzáles";
        Date fechaNacimientoVacunado = new Date();
        String IdVacunaccion = "2G2SJ8C6BD";
        String nacionalidad = "Mexicana";
        String estadoNacimiento = "Ciudad de México";
        String sexo = "mujer";
        String curpVacunado = "JIHF121111MDFMRR";
        //String domicilio = "Canadá N0. Ext. 94, N0. Int 002, Parque San Andres, Coyoacán, CDMX.";
        String domicilio = "Canadá N0. Ext. 94, N0. Int 002,";

        //obtener info Vac
        obtenerInformacionVacunado(nombreVacunado, fechaNacimientoVacunado, IdVacunaccion, nacionalidad, estadoNacimiento, sexo, curpVacunado, domicilio);

        //_______________________________________________________________________________________

        String nombreResponsable = "Rosa María Gonzáles Benitez";
        String parentesco = "Madre";
        Date fechaNacimientoResponsable = new Date();
        String curpResponsable = "GOBR891211MDFMRR";

        //obtener info Res
        obtenerInformacionResponsable(nombreResponsable, parentesco, fechaNacimientoResponsable, curpResponsable);


        //----------------------------------------------------------------
        //                DIBUJA IMAGEN CARTILLA ANDROID
        //----------------------------------------------------------------

        //Dibujar informacion del vacunado
        dibujarInfonacionPersonalVacunado(canvas);

        //Dibujar informacion del Responsable
        dibujaInformacionResponsables(canvas);

        //Dibujar Lineas de separacion
        dubujaLineasSeparacionCartilla(canvas);

        //Dibujar parte central
        dibujarParteCentral(canvas);

        //Dibujar parte derecha
        dibujarParteDerecha(canvas);

        //Imagen Cartilla Exportar la imagen
        try {
            File directory = new File("/storage/emulated/0/Download/");
            File archive = new File(directory, "vaccination_card.jpg");

            if (archive.exists()) {
                archive.delete();
            }

            OutputStream outputStream = new FileOutputStream(archive);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);
            outputStream.flush();
            outputStream.close();

        } catch (Exception e) {
            Log.e("Error--------->", e.toString());
            Log.e("Log consola","Fallo en generacion de Imagen Cartilla");
        }
    }

    //----------------------------------------------------------------------------------------------------------
    //                                   GENERA PDF CARTILLA
    //----------------------------------------------------------------------------------------------------------
    public void exportaTestPDF() {
        Log.d("Cartilla", "Entro al metodo que genera PDF Cartilla");

        //crear Documento PDF
        PdfDocument document = new PdfDocument();

        //crear una pagina de Descripcion
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo.Builder(3507, 2480,1).create();

        //Inicializar pagina
        PdfDocument.Page page = document.startPage(pageInfo);

        //Crear canvas para PDF
        Canvas canvas = page.getCanvas();


        //Inicia Imagen __________________________________________________
        canvas.drawColor(getResources().getColor(R.color.FondoCartillaColor));

        String nombreVacunado = "María Luisa Ortiz Gonzáles";
        Date fechaNacimientoVacunado = new Date();
        String IdVacunaccion = "2G2SJ8C6BD";
        String nacionalidad = "Mexicana";
        String estadoNacimiento = "Ciudad de México";
        String sexo = "hombre";
        String curpVacunado = "JIHF121111MDFMRR";
        //String domicilio = "Canadá N0. Ext. 94, N0. Int 002, Parque San Andres, Coyoacán, CDMX.";
        String domicilio = "Canadá N0. Ext. 94, N0. Int 002,";

        //obtener info Vac
        obtenerInformacionVacunado(nombreVacunado, fechaNacimientoVacunado, IdVacunaccion, nacionalidad, estadoNacimiento, sexo, curpVacunado, domicilio);
        //_______________________________________________________________________________________

        String nombreResponsable = "Rosa María Gonzáles Benitez";
        String parentesco = "Madre";
        Date fechaNacimientoResponsable = new Date();
        String curpResponsable = "GOBR891211MDFMRR";

        //obtener info Res
        obtenerInformacionResponsable(nombreResponsable, parentesco, fechaNacimientoResponsable, curpResponsable);


        //----------------------------------------------------------------
        //                PDF CARTILLA ANDROID
        //----------------------------------------------------------------

        //Dibujar informacion del vacunado
        dibujarInfonacionPersonalVacunado(canvas);

        //Dibujar informacion del Responsable
        dibujaInformacionResponsables(canvas);

        //Dibujar Lineas de separacion
        dubujaLineasSeparacionCartilla(canvas);

        //Dibujar parte central
        dibujarParteCentral(canvas);

        //Dibujar parte derecha
        dibujarParteDerecha(canvas);

        //Cierra Imagen __________________________________________________
        document.finishPage(page);


        //Pagina Documento DOS imagen pagina Dos
        pageInfo = new PdfDocument.PageInfo.Builder(3507, 2480,2).create();

        page = document.startPage(pageInfo);
        canvas = page.getCanvas();

        //Llama imagen Dos
        imagenFinal(canvas);

        //Cierra PDF
        document.finishPage(page);

        //EXPORTA DOCUMENTO PDF CON INMAGEN CARTILLA
        try {
            File directory = new File("/storage/emulated/0/Download/");
            File archive = new File(directory, "vaccination_PdfDocumente.pdf");

            if (archive.exists()) {
                archive.delete();
            }

            document.writeTo(new FileOutputStream(archive));
            document.close();

        } catch (Exception e) {
            Log.e("Error--------->", e.toString());
            Log.e("Log consola","Fallo al generar Documento PDF");
        }

    }


    //------------------------------- VACUNADO  ----------------------------------------------------------------------
    private String nombreVacunado;
    private Date fechaNacimientoVacunado;
    private String IdVacunaccion;
    private String nacionalidad;
    private String estadoNacimiento;
    private String sexo;
    private String curpVacunado;
    private String domicilio;

    public void  obtenerInformacionVacunado(String nombreVacunado,
                                       Date fechaNacimientoVacunado,
                                       String IdVacunaccion,
                                       String nacionalidad,
                                       String estadoNacimiento,
                                       String sexo,
                                       String curpVacunado,
                                       String domicilio) {

        this.nombreVacunado = nombreVacunado;
        this.fechaNacimientoVacunado = fechaNacimientoVacunado;
        this.IdVacunaccion = IdVacunaccion;
        this.nacionalidad = nacionalidad;
        this.estadoNacimiento = estadoNacimiento;
        this.sexo = sexo;
        this.curpVacunado = curpVacunado;
        this.domicilio = domicilio;

    }


    //------------------------------- RESPONSABLE  -------------------------------------------------------------------
    private String nombreResponsable;
    private String parentesco;
    private Date fechaNacimientoResponsable;
    private String curpResponsable;

    public void obtenerInformacionResponsable(String nombreResponsable,
                                           String parentesco,
                                           Date fechaNacimientoResponsable,
                                           String curpResponsable) {

        this.nombreResponsable = nombreResponsable;
        this.parentesco = parentesco;
        this.fechaNacimientoResponsable = fechaNacimientoResponsable;
        this.curpResponsable = curpResponsable;

    }

    //---------------------------------------------------------------------------------------
    //                                       VACUNADO
    //---------------------------------------------------------------------------------------
    private void pintaInformacionDeVacunado(Canvas canvas) {
        Log.d("Log consola","muestra informacion de los vacunados ");

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;

        //-----INFORMACION DEL VACUNADO------------
        Paint pintaInformacionVacunado = new Paint();
        pintaInformacionVacunado.setTextSize(54);
        pintaInformacionVacunado.setAntiAlias(true);
        pintaInformacionVacunado.setColor(getResources().getColor(R.color.textoNegro));
        Typeface infoBol = ResourcesCompat.getFont(this,R.font.bold);
        pintaInformacionVacunado.setTypeface(infoBol);

        //-----NOMBRRE DEL VACUNADO------------
        Paint nombresVacunado = new Paint();
        nombresVacunado.setTextSize(47);
        nombresVacunado.setAntiAlias(true);
        nombresVacunado.setColor(getResources().getColor(R.color.textoNegro));
        Typeface infoMed = ResourcesCompat.getFont(this,R.font.medium);
        nombresVacunado.setTypeface(infoMed);

        //-----NOMBRRE DEL VACUNADO------------
        Paint infoPersonal = new Paint();
        infoPersonal.setTextSize(49);
        infoPersonal.setAntiAlias(true);
        infoPersonal.setColor(getResources().getColor(R.color.textoVerde));
        Typeface infoPer = ResourcesCompat.getFont(this,R.font.medium);
        infoPersonal.setTypeface(infoPer);

        //-----MUESTRA INFO VACUNADO ESTATICO------------
        Paint tipoInfoVacunado = new Paint();
        tipoInfoVacunado.setTextSize(43);
        tipoInfoVacunado.setAntiAlias(true);
        tipoInfoVacunado.setColor(getResources().getColor(R.color.textoGris));
        Typeface infoVacEstatico = ResourcesCompat.getFont(this,R.font.regular);
        tipoInfoVacunado.setTypeface(infoVacEstatico);

        //-----MUESTRA INFO VACUNADO DINAMICO------------
        Paint datosVacunado = new Paint();
        datosVacunado.setTextSize(41);
        datosVacunado.setAntiAlias(true);
        datosVacunado.setColor(getResources().getColor(R.color.textoNegro));
        Typeface infoVac = ResourcesCompat.getFont(this,R.font.medium);
        datosVacunado.setTypeface(infoVac);

        //Informacion Vacunados
        canvas.drawText(getString(R.string.infoVacunadoTexto), 295,170, pintaInformacionVacunado);

        //Nombre Vacunado
        canvas.drawText(nombreVacunado, 310,640, nombresVacunado);
        //canvas.drawText(nombreVacunado, 310,685, nombresVacunado);

        //Info Personal
        canvas.drawText(getString(R.string.infoPersonalTexto),250,760,infoPersonal);

        //Fecha Nacimiento
        canvas.drawText(getString(R.string.fechaNacimientoTexto),250,845,tipoInfoVacunado);
        DateFormat dateFormatFecha = new SimpleDateFormat("dd/MM/yyyy");
        canvas.drawText(dateFormatFecha.format(fechaNacimientoVacunado),250,893,datosVacunado);

        //ID VacunAccion
        canvas.drawText(getString(R.string.idVacunaccionTexto),250,970,tipoInfoVacunado);
        canvas.drawText(IdVacunaccion,250,1018,datosVacunado);

        //Nacionalidad
        canvas.drawText(getString(R.string.nacionalidadTexto),250,1095,tipoInfoVacunado);
        canvas.drawText(nacionalidad,250,1143,datosVacunado);

        //Estado Nacimiento
        canvas.drawText(getString(R.string.estadoNacimientoTexto),250,1220,tipoInfoVacunado);
        canvas.drawText(estadoNacimiento,250,1268,datosVacunado);

        //Sexo
        canvas.drawText(getString(R.string.sexoTexto),250,1345,tipoInfoVacunado);
        canvas.drawText(sexo,250,1393,datosVacunado);

        //Curp
        canvas.drawText(getString(R.string.curpTexto),250,1470,tipoInfoVacunado);
        canvas.drawText(curpVacunado,250,1518,datosVacunado);

        //VALIDAR DOMICILIO

        if (domicilio.length() > 31) {

        }

        //Domicilio
        canvas.drawText(getString(R.string.domicilioTexto),250,1595,tipoInfoVacunado);
        canvas.drawText(domicilio,250,1643,datosVacunado);
        //canvas.drawText(domicilio,250,1688,datosVacunado);

    }


    //-------------------------------------------------------------------------------------------------------------
    //                                 RESPONSABLES
    //-------------------------------------------------------------------------------------------------------------
    //info Responsable
    private void pintaInformacionDeResponsable(Canvas canvas) {
        int alturaCartilla = 2480;

        //-----INFORMACION------------
        Paint informacionResponsable = new Paint();
        informacionResponsable.setTextSize(49);
        informacionResponsable.setAntiAlias(true);
        informacionResponsable.setColor(getResources().getColor(R.color.textoVerde));
        Typeface boldVacunaR = ResourcesCompat.getFont(this,R.font.medium);
        informacionResponsable.setTypeface(boldVacunaR);

        //-----TEXTO ESTATICO------------
        Paint fecha_Curp = new Paint();
        fecha_Curp.setTextSize(43);
        fecha_Curp.setAntiAlias(true);
        fecha_Curp.setColor(getResources().getColor(R.color.textoGris));
        Typeface boldVacunaResEs = ResourcesCompat.getFont(this,R.font.regular);
        fecha_Curp.setTypeface(boldVacunaResEs);

        //-----TEXTO DINAMICO------------
        Paint datosResponsable = new Paint();
        datosResponsable.setTextSize(41);
        datosResponsable.setAntiAlias(true);
        datosResponsable.setColor(getResources().getColor(R.color.textoNegro));
        Typeface boldVacunaRes = ResourcesCompat.getFont(this,R.font.medium);
        datosResponsable.setTypeface(boldVacunaRes);


        //Responsable
        canvas.drawText(getString(R.string.infoResponsablesTexto), 250,alturaCartilla-550, informacionResponsable);

        //Nombre del Responsable
        canvas.drawText(nombreResponsable,250,alturaCartilla-450,datosResponsable);
        //Tipo de Parentesco
        canvas.drawText(parentesco,250,alturaCartilla-402,datosResponsable);

        //Fecha de Nacimiento
        canvas.drawText(getString(R.string.fechaNacimientoTexto),250,alturaCartilla-305,fecha_Curp);
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        canvas.drawText( dateFormat.format(fechaNacimientoResponsable) ,250,alturaCartilla-257,datosResponsable);

        //CURP
        canvas.drawText(getString(R.string.curpTexto),250,alturaCartilla-160,fecha_Curp);
        canvas.drawText(curpResponsable,250,alturaCartilla-112,datosResponsable);

    }

    //-------------------------------------------------------------------------------------------------------------
    //                              PINTAR FECHAS CARTILLA COMPLETA
    //-------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------------------------------------------
    //                                 VACUNAS FECHAS CENTRAL
    //-------------------------------------------------------------------------------------------------------------
    //METODO PARA PINTAR LA INFORMACIO DE FECHAS DINAMICAS COLUMNA CENTRAL ----------------------------------------
    private void pintaFechasVacunasCentral(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint mesesVacunas = new Paint();
        mesesVacunas.setTextSize(31);
        mesesVacunas.setAntiAlias(true);
        mesesVacunas.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldNegritaFechas = ResourcesCompat.getFont(this,R.font.bold);
        mesesVacunas.setTypeface(boldNegritaFechas);


        String fecha = "40 Nov 2020";
        int equivalenteNeumococicaPrimera =1;

        //BCG Vacuna _______________________________________________________________________________________________
        canvas.drawText(fecha, startX+907,alturaCentro-2000, mesesVacunas);


        //HEPATITIS Vacuna _________________________________________________________________________________________
        //HEPATITIS primera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1820, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1820, mesesVacunas);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1820, mesesVacunas);
                circuloHepapitisPrimera(canvas);
            }
        }

        //HEPATITIS Segunda
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1650, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1650, mesesVacunas);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1650, mesesVacunas);
                circuloHepapitisSegunda(canvas);
            }
        }


        //HEPATITIS Tercera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1480, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1480, mesesVacunas);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1480, mesesVacunas);
                circuloHepapitisTercera(canvas);

            }
        }

        //PENTAVALENTE ACELULAR Vacuna ______________________________________________________________________________
        //PENTAVALENTE primera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1325, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1325, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1325, mesesVacunas);
                circuloPentavalentePrimera(canvas);
            }
        }

        //PENTAVALENTE Segunda
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1175, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1175, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1175, mesesVacunas);
                circuloPentavalenteSegunda(canvas);
            }
        }


        //PENTAVALENTE Tercera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-1025, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-1025, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-1025, mesesVacunas);
                circuloPentavalenteTercera(canvas);
            }
        }

        //PENTAVALENTE Cuarta
        if(fecha == null) {
            canvas.drawText(fecha, startX+904,alturaCentro-890, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-890, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-890, mesesVacunas);
                circuloPentavalenteCuarta(canvas);
            }
        }


        //DTP REFUERZO Vacuna _______________________________________________________________________________________
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-630, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-630, mesesVacunas);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-630, mesesVacunas);
                circuloDTPRefuerzo1(canvas);
            }else if(equivalenteNeumococicaPrimera == 2){
                canvas.drawText(fecha, startX+904,alturaCentro-630, mesesVacunas);
                circuloDTPRefuerzo2(canvas);
            }
        }


        //ROTAVIRUAS Vacuna _________________________________________________________________________________________
        //Rotavirus Primera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-360, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-360, mesesVacunas);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-360, mesesVacunas);
                circuloRotavirusPrimera1(canvas);

            }else if(equivalenteNeumococicaPrimera == 2){
                canvas.drawText(fecha, startX+904,alturaCentro-360, mesesVacunas);
                circuloRotavirusPrimera2(canvas);
            }
        }

        //Rotavirus Segunda
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-210, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-210, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-210, mesesVacunas);
                circuloRotavirusSegunda1(canvas);
            }else if(equivalenteNeumococicaPrimera == 2){
                canvas.drawText(fecha, startX+904,alturaCentro-210, mesesVacunas);
                circuloRotavirusSegunda2(canvas);
            }
        }

        //Rotavirus Tercera
        if(fecha == null) {
            canvas.drawText("", startX+904,alturaCentro-63, mesesVacunas);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha, startX+904,alturaCentro-63, mesesVacunas);
            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha, startX+904,alturaCentro-63, mesesVacunas);
                circuloRotavirusTercera1(canvas);

            }else if(equivalenteNeumococicaPrimera == 2){
                canvas.drawText(fecha, startX+904,alturaCentro-63, mesesVacunas);
                circuloRotavirusTercera2(canvas);

            }
        }

    }

    //-------------------------------------------------------------------------------------------------------------
    //                                 VACUNAS FECHAS DERECHA
    //-------------------------------------------------------------------------------------------------------------
     //FECHAS Y CIRCULOS COLUMNA 3
    //METODO PARA PINTAR LA INFORMACIO DE FECHAS DINAMICAS COLUMNA DERECHA ----------------------------------------
    private void pintaFechasVacunasDerecha(Canvas canvas) {

        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla / 3) * 2;
        int inioLimite = lstartX + 50;
        int alturaCentro = 2420;

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA MESES ------------
        Paint mesesVacunas = new Paint();
        mesesVacunas.setTextSize(37);
        mesesVacunas.setAntiAlias(true);
        mesesVacunas.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldNegritaFechas = ResourcesCompat.getFont(this, R.font.bold);
        mesesVacunas.setTypeface(boldNegritaFechas);

        Paint mesesVacunasNe = new Paint();
        mesesVacunasNe.setTextSize(32);
        mesesVacunasNe.setAntiAlias(true);
        mesesVacunasNe.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this, R.font.bold);
        mesesVacunasNe.setTypeface(boldNegritaFechas1);

        String fecha = "13/06/2019";
        String fecha1 = "22 feb 2017";
        //String fecha2 = null;
        String fecha2 = "22 feb 2017";
        int equivalenteNeumococicaPrimera = 1;
        //canvas.drawText(fecha, inioLimite+860,alturaCentro-2010, mesesVacunas1);

        //NEUMOCOCICA PRIMERA ______________________________________________________________________________________
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 2010, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 2010, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 2010, mesesVacunasNe);
                circuloNeumococicaPrimera(canvas);

            } else if (equivalenteNeumococicaPrimera == 2) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 2010, mesesVacunasNe);
                circuloNeumococicaSegunda(canvas);

            }
        }

        //NEUMOCOCICA SEGUNDA
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 1830, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1830, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1830, mesesVacunasNe);
                circuloNeumococicaSegunda1(canvas);

            } else if (equivalenteNeumococicaPrimera == 2) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1830, mesesVacunasNe);
                circuloNeumococicaSegunda2(canvas);

            }
        }

        //NEUMOCOCICA REFUERZO
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 1660, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1660, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1660, mesesVacunasNe);
                circuloNeumococicaRefuerzo1(canvas);

            } else if (equivalenteNeumococicaPrimera == 2) {

                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1660, mesesVacunasNe);
                circuloNeumococicaRefuerzo2(canvas);

            }
        }

        //______________________________________________________________________________________________________
        //INFLUENZA PRIMERA
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 1480, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1480, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1480, mesesVacunasNe);
                circuloInfluenzaPrimera1(canvas);
            }
        }

        //INFLUENZA SEGUNDA
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 1310, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1310, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1310, mesesVacunasNe);
                circuloInfluenzaSegunda1(canvas);
            }
        }

        //INFLUENZA ADICIONAL
        if (fecha2 == null) {
            canvas.drawText("", inioLimite + 857, alturaCentro - 1150, mesesVacunasNe);
        } else {

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1150, mesesVacunasNe);

            } else if (equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite + 857, alturaCentro - 1150, mesesVacunasNe);
                circuloInfluenzaAdicional1(canvas);
            }

        }

        //______________________________________________________________________________________________________
        //SRP PRIMERA
        if(fecha2 == null) {
            canvas.drawText("", inioLimite+857,alturaCentro-970, mesesVacunasNe);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite+857,alturaCentro-970, mesesVacunasNe);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite+857,alturaCentro-970, mesesVacunasNe);
                circuloSRPPrimera(canvas);
            }
        }

        //SRP REFUERZO
        if(fecha2 == null) {
            canvas.drawText("", inioLimite+857,alturaCentro-770, mesesVacunasNe);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite+857,alturaCentro-770, mesesVacunasNe);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite+857,alturaCentro-770, mesesVacunasNe);
                circuloSRPRefuerzo(canvas);
            }
        }


        //________________________________________________________________________________________________
        //ANTIPOLIOMIELITICA
        canvas.drawText(fecha1, inioLimite+857,alturaCentro-495, mesesVacunasNe);

        //________________________________________________________________________________________________
        //SR VACUNA
        if(fecha2 == null) {
            canvas.drawText("", inioLimite+860,alturaCentro-160, mesesVacunasNe);
        }else{

            if (equivalenteNeumococicaPrimera == 0) {
                canvas.drawText(fecha1, inioLimite+860,alturaCentro-160, mesesVacunasNe);

            }else if(equivalenteNeumococicaPrimera == 1) {
                canvas.drawText(fecha1, inioLimite+860,alturaCentro-160, mesesVacunasNe);
                circuloSRAdicional(canvas);
            }
        }

    }


    //---------------------------------------------------------------------------------------
    //                       PINTA CIRCULOS VACUNAS 1 - 5 COLUMNA 2 CARTILLA
    //---------------------------------------------------------------------------------------
    //pintarcirculos de vacunas central
    //Hepapitis primera
    private void circuloHepapitisPrimera(Canvas canvas){
        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.HepatitisBColor));

        //canvas.drawText(fecha, startX+907,alturaCentro-1820, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1833, 15, vacuna2);
    }

    //Hepapitis primera
    private void circuloHepapitisSegunda(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.HepatitisBColor));

        //canvas.drawText(fecha, startX+907,alturaCentro-1650, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1663, 15, vacuna2);
    }

    //Hepapitis primera
    private void circuloHepapitisTercera(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;


        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.HepatitisBColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-1480, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1493, 15, vacuna2);

    }

    //Pentavalente ____________________________________________________________________________________
    //Pentavalente Primera
    private void circuloPentavalentePrimera(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;


        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-1325, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1338, 15, vacuna2);

    }

    //Pentavalente Segunda
    private void circuloPentavalenteSegunda(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;


        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-1175, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1188, 15, vacuna2);

    }

    //Pentavalente Tercera
    private void circuloPentavalenteTercera(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;


        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-1025, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-1038, 15, vacuna2);

    }

    //Pentavalente Cuarta
    private void circuloPentavalenteCuarta(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;


        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-890, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-903, 15, vacuna2);

    }

    //DTP ____________________________________________________________________________________
    //DTP
    private void circuloDTPRefuerzo1(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.DTPRefuerzoColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-630, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-643, 15, vacuna2);

        pintaSobreCirculoDTP1(canvas);
    }

    private void circuloDTPRefuerzo2(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.DTPRefuerzoColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-630, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-643, 15, vacuna2);

        pintaSobreCirculoDTP2(canvas);
    }

    //Letra Refuerzo 1
    private void pintaSobreCirculoDTP1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-643, 15, vacuna2);
        canvas.drawText(getString(R.string.unoNumero), startX +1090,alturaCentro-632, tipoNumero);
    }

    //Letra Refuerzo 2
    private void pintaSobreCirculoDTP2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-643, 15, vacuna2);
        canvas.drawText(getString(R.string.dosNumero), startX +1090,alturaCentro-632, tipoNumero);
    }


    //Rotavirus ____________________________________________________________________________________
    //Rotavirus Primera
    private void circuloRotavirusPrimera1(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-360, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-373, 15, vacuna2);
        pintaSobreCirculoRotavirusPrimera1(canvas);
    }

    //Rotavirus Primera
    private void circuloRotavirusPrimera2(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-360, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-373, 15, vacuna2);

        pintaSobreCirculoRotavirusPrimera2(canvas);
    }

    //Letra Rotavirus 1
    private void pintaSobreCirculoRotavirusPrimera1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-373, 15, vacuna2);
        canvas.drawText(getString(R.string.unoNumero), startX +1090,alturaCentro-362, tipoNumero);
    }

    //Letra Rotavirus 2
    private void pintaSobreCirculoRotavirusPrimera2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-373, 15, vacuna2);
        canvas.drawText(getString(R.string.dosNumero), startX +1090,alturaCentro-362, tipoNumero);
    }


    //Rotavirus Segunda
    private void circuloRotavirusSegunda1(Canvas canvas) {

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-210, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-223, 15, vacuna2);

        pintaSobreCirculoRotavirusSegunda1(canvas);
    }

    //Rotavirus Segunda
    private void circuloRotavirusSegunda2(Canvas canvas) {

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-210, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-223, 15, vacuna2);

        pintaSobreCirculoRotavirusSegunda2(canvas);
    }


    //Letra Rotavirus 1 Segunda
    private void pintaSobreCirculoRotavirusSegunda1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-223, 15, vacuna2);
        canvas.drawText(getString(R.string.unoNumero), startX +1090,alturaCentro-212, tipoNumero);
    }

    //Letra Rotavirus 2 Segunda
    private void pintaSobreCirculoRotavirusSegunda2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-223, 15, vacuna2);
        canvas.drawText(getString(R.string.dosNumero), startX +1090,alturaCentro-212, tipoNumero);
    }


    //Rotavirus Tercera
    private void circuloRotavirusTercera1(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-63, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-76, 15, vacuna2);

        pintaSobreCirculoRotavirusTercera1(canvas);
    }

    //Rotavirus Tercera
    private void circuloRotavirusTercera2(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.RotavirusColor));

        //canvas.drawText(fecha, startX+904,alturaCentro-63, mesesVacunas);
        canvas.drawCircle( startX+1101, alturaCentro-76, 15, vacuna2);

        pintaSobreCirculoRotavirusTercera2(canvas);
    }

    //Letra Rotavirus 2 Segunda
    private void pintaSobreCirculoRotavirusTercera1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-76, 15, vacuna2);
        canvas.drawText(getString(R.string.unoNumero), startX +1090,alturaCentro-64, tipoNumero);
    }

    //Letra Rotavirus 2 Segunda
    private void pintaSobreCirculoRotavirusTercera2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(34);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        //canvas.drawCircle( startX+1101, alturaCentro-76, 15, vacuna2);
        canvas.drawText(getString(R.string.dosNumero), startX +1090,alturaCentro-64, tipoNumero);
    }




    //---------------------------------------------------------------------------------------
    //                       PINTA CIRCULOS VACUNAS 6 - 10 COLUMNA 3 CARTILLA
    //---------------------------------------------------------------------------------------
    //----------- NEUMOCOCICA PRIMERA--------------------------------------------------------
    //Circulo vacuna NEUMOCOCICA PRIMERA
    private void circuloNeumococicaPrimera(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna5 = new Paint();
        vacuna5.setStyle(Paint.Style.FILL);
        vacuna5.setStrokeWidth(0);
        vacuna5.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha1, inioLimite+855,alturaCentro-2010, mesesVacunas1);
        canvas.drawCircle( inioLimite+1052, alturaCentro-2025, 16, vacuna5);

        pintaSobreCirculoNeumococicaPrimera(canvas);
    }

    //Circulo vacuna NEUMOCOCICA SEGUNDA
    private void circuloNeumococicaSegunda(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna5 = new Paint();
        vacuna5.setStyle(Paint.Style.FILL);
        vacuna5.setStrokeWidth(0);
        vacuna5.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha1, inioLimite+855,alturaCentro-2010, mesesVacunas1);

        canvas.drawCircle( inioLimite+1052, alturaCentro-2025, 16, vacuna5);
        pintaSobreCirculoNeumococicaSegunda(canvas);
    }

    //primera Neuomcocica
    private void pintaSobreCirculoNeumococicaPrimera(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.unoNumero), inioLimite +1043,alturaCentro-2012, tipoNumero);
    }

    //Segunda Neuomcocica
    private void pintaSobreCirculoNeumococicaSegunda(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.dosNumero), inioLimite +1043,alturaCentro-2012, tipoNumero);
    }

    //----------- NEUMOCOCICA SEGUNDA--------------------------------------------------------
    //Circulo vacuna NEUMOCOCICA segunda
    private void circuloNeumococicaSegunda1(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha, inioLimite+857,alturaCentro-1830, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1843, 16, vacuna6);

        pintaSobreCirculoNeumococicaSegunda1(canvas);
    }

    //Circulo vacuna NEUMOCOCICA SEGUNDA
    private void circuloNeumococicaSegunda2(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha, inioLimite+857,alturaCentro-1830, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1843, 16, vacuna6);

        pintaSobreCirculoNeumococicaSegunda2(canvas);
    }

    //primera Neuomcocica
    private void pintaSobreCirculoNeumococicaSegunda1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.unoNumero), inioLimite +1043,alturaCentro-1831, tipoNumero);
    }

    //Segunda Neuomcocica
    private void pintaSobreCirculoNeumococicaSegunda2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.dosNumero), inioLimite +1043,alturaCentro-1831, tipoNumero);
    }

    //----------- NEUMOCOCICA REFUERZO--------------------------------------------------------
    //Circulo vacuna NEUMOCOCICA segunda
    private void circuloNeumococicaRefuerzo1(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-1660, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1673, 16, vacuna6);

        pintaSobreCirculoNeumococicaRefuerzo1(canvas);
    }

    //Circulo vacuna NEUMOCOCICA SEGUNDA
    private void circuloNeumococicaRefuerzo2(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        //canvas.drawText(fecha, inioLimite+857,alturaCentro-1830, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1673, 16, vacuna6);

        pintaSobreCirculoNeumococicaRefuerzo2(canvas);
    }

    //primera Neuomcocica
    private void pintaSobreCirculoNeumococicaRefuerzo1(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.unoNumero), inioLimite +1043,alturaCentro-1660, tipoNumero);
    }

    //Segunda Refuerzo
    private void pintaSobreCirculoNeumococicaRefuerzo2(Canvas canvas) {
        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));
        Typeface boldNegritaFechas1 = ResourcesCompat.getFont(this,R.font.bold);
        tipoNumero.setTypeface(boldNegritaFechas1);

        canvas.drawText(getString(R.string.dosNumero), inioLimite +1043,alturaCentro-1660, tipoNumero);
    }

    //----------- CIRCULO INFLUENZA VACUNA--------------------------------------------------------
    //----------- --------------------------------------------------------------------------------
    //Circulo vacuna INFLUENZA PRIMERA
    private void circuloInfluenzaPrimera1(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.InfluenzaColor));

        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-1480, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1493, 16, vacuna6);

    }


    //Circulo vacuna INFLUENZA segunda --------------------------------------------------------------------
    private void circuloInfluenzaSegunda1(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.InfluenzaColor));


        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-1310, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1323, 16, vacuna6);

    }

    //Circulo vacuna INFLUENZA ADICIONAL --------------------------------------------------------------------
    private void circuloInfluenzaAdicional1(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.InfluenzaColor));

        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-1150, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-1163, 16, vacuna6);
    }

    //----------- CIRCULO SRP VACUNA--------------------------------------------------------
    //----------- --------------------------------------------------------------------------------
    //Circulo vacuna SRP PRIMERA
    private void circuloSRPPrimera(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.SRPColor));

        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-970, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-983, 16, vacuna6);

    }

    //Circulo vacuna SRP REfuerzo
    private void circuloSRPRefuerzo(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.SRPColor));

        //canvas.drawText(fecha1, inioLimite+857,alturaCentro-770, mesesVacunasNe);
        canvas.drawCircle( inioLimite+1052, alturaCentro-783, 16, vacuna6);
    }

    //----------- CIRCULO SR VACUNA--------------------------------------------------------
    //----------- -------------------------------------------------------------------------
    //Circulo vacuna SR Adicional
    private void circuloSRAdicional(Canvas canvas) {

        int anchoCartilla = 3507;
        int alturaCentro = 2420;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.SRColor));

        //canvas.drawText(fecha1, inioLimite+860,alturaCentro-160, mesesVacunas);
        canvas.drawCircle( inioLimite+1052, alturaCentro-173, 16, vacuna6);

    }


    //-------------------------------------------------------------------------------------------------------------
    // DISEÑO DE CARTILLA   DISEÑO DE CARTILLA
    //-------------------------------------------------------------------------------------------------------------
    //METODO PARA PINTAR IMAGEN -----------------------------------------------------------------------
    private void pintaImagen(Canvas canvas){

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int startXs = startX-50;
        int limiteRectangulo = alturaCartilla-680;

        if (sexo.equalsIgnoreCase("hombre")){
            //Drawable imagen = getDrawable(R.drawable.children2);
            Drawable imagen = getDrawable(R.drawable.children7);

            //imagen.setBounds(380,210,startXs-330,limiteRectangulo-1190);
            imagen.setBounds(400,210,startXs-350,limiteRectangulo-1230);
            imagen.draw(canvas);
        } else {
            //Drawable imagen = getDrawable(R.drawable.children2);
            Drawable imagen = getDrawable(R.drawable.children2);

            //imagen.setBounds(380,210,startXs-330,limiteRectangulo-1190);
            imagen.setBounds(400,210,startXs-350,limiteRectangulo-1230);
            imagen.draw(canvas);
        }

    }


    //LINEAS SEPARACION
    private void dubujaLineasSeparacionCartilla(Canvas canvas) {

        int alturaCartilla = 2480;
        int anchoCartilla = 3507;
        int startX = anchoCartilla/3;
        int startY = 0;
        int stopX = anchoCartilla/3;
        int stopY = alturaCartilla;
        int lstartX = (anchoCartilla/3) * 2;
        int lstartY = 0;
        int lstopX = (anchoCartilla/3) * 2;
        int lstopY = alturaCartilla;

        //--LINEA IZQUIERDA-------------------------------------------------------------------
        Paint paintDash = new Paint();
        paintDash.setColor(getResources().getColor(R.color.lineasPunteadas));
        paintDash.setStyle(Paint.Style.STROKE);
        paintDash.setPathEffect(new DashPathEffect(new float[]{35f,15f}, 0));
        paintDash.setStrokeWidth(2);

        Path pathDashLine = new Path();
        pathDashLine.reset();
        pathDashLine.moveTo(startX, startY);
        pathDashLine.lineTo(stopX,stopY);
        canvas.drawPath(pathDashLine, paintDash);

        //--LINEA DERECHA---------------------------------------------------------------------
        Paint paintDashDerecha = new Paint();
        paintDashDerecha.setColor(getResources().getColor(R.color.lineasPunteadas));
        paintDashDerecha.setStyle(Paint.Style.STROKE);
        paintDashDerecha.setPathEffect(new DashPathEffect(new float[]{35f,15f}, 0));
        paintDashDerecha.setStrokeWidth(2);

        Path pathDashLine1 = new Path();
        pathDashLine1.reset();
        pathDashLine1.moveTo(lstartX, lstartY);
        pathDashLine1.lineTo(lstopX,lstopY);
        canvas.drawPath(pathDashLine1, paintDashDerecha);

    }

    //-------------------------------------------------------------------------------------------------------------
    //DESPLIEGA INFORMACION DEL VACUNADO
    private void dibujarInfonacionPersonalVacunado(Canvas canvas) {
        Log.d("Lienzo","Entro Parte izquierda con curva");

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;

        Paint pintaRectanguloBorde = new Paint();
        pintaRectanguloBorde.setStyle(Paint.Style.FILL);
        pintaRectanguloBorde.setStrokeWidth(0);
        pintaRectanguloBorde.setColor(getResources().getColor(R.color.colorInformacionVacunadoResponsables));

        RectF informacionVacunado = new RectF(50,alturaCartilla-680, startX-50,60);
        canvas.drawRoundRect(informacionVacunado,50,50,pintaRectanguloBorde);

        //PINTA INFORMAACION VACUNADO
        pintaInformacionDeVacunado(canvas);

        //PINTA IMAGEN
        pintaImagen(canvas);

    }


    //DIBUJA RESPONSABLES -----------------------------------------------------------------------------------------
    private void dibujaInformacionResponsables(Canvas canvas) {

        Log.d("Lienzo","Informacion Responsables");

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;

        Paint pintaResponsable = new Paint();
        pintaResponsable.setStyle(Paint.Style.FILL);
        pintaResponsable.setStrokeWidth(0);
        pintaResponsable.setColor(getResources().getColor(R.color.colorInformacionVacunadoResponsables));

        RectF informacionResponsables = new RectF(50,alturaCartilla-640,startX-50,alturaCartilla-60);
        canvas.drawRoundRect(informacionResponsables,50,50,pintaResponsable);

        //Pinta Informacion de Responsables
        pintaInformacionDeResponsable(canvas);

    }

    //METODO CENTRAL-----------------------------------------------------------------------------------------------
    private void dibujarParteCentral(Canvas canvas) {
        Log.d("Lienzo","Parte Central");

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;

        int startX = anchoCartilla/3;
        int lstartX = (anchoCartilla/3) * 2;

        Paint pintaCentra = new Paint();
        pintaCentra.setStyle(Paint.Style.FILL);
        pintaCentra.setStrokeWidth(0);
        pintaCentra.setColor(getResources().getColor(R.color.tiposVacunas));

        RectF parteCentral = new RectF(startX+50,alturaCartilla-60, lstartX-50,60);
        canvas.drawRoundRect(parteCentral,50,50 ,pintaCentra);

            //RECTANGULOS CENTRALES -------------------------
        pintaRectanguloconBordes(canvas);

    }


    //RESCTANGULOS CENTRALES --------------------------------------------------------------------------------------
    private void pintaRectanguloconBordes(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int lstartX = (anchoCartilla/3) * 2;
        int alturaCentro = 2420;

        Paint pintaCentra = new Paint();
        pintaCentra.setStyle(Paint.Style.FILL);
        pintaCentra.setStrokeWidth(0);
        pintaCentra.setColor(getResources().getColor(R.color.colorCartilla));

        Paint pintapinta1 = new Paint();
        pintapinta1.setStyle(Paint.Style.FILL);
        pintapinta1.setStrokeWidth(0);
        pintapinta1.setColor(getResources().getColor(R.color.encabezado));

        Paint pintapinta2 = new Paint();
        pintapinta2.setStyle(Paint.Style.FILL);
        pintapinta2.setStrokeWidth(0);
        pintapinta2.setColor(getResources().getColor(R.color.colorInformacionVacunas));

        Paint vacuna1 = new Paint();
        vacuna1.setStyle(Paint.Style.FILL);
        vacuna1.setStrokeWidth(0);
        vacuna1.setColor(getResources().getColor(R.color.CBGColor));

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.HepatitisBColor));

        Paint vacuna3 = new Paint();
        vacuna3.setStyle(Paint.Style.FILL);
        vacuna3.setStrokeWidth(0);
        vacuna3.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        Paint vacuna4 = new Paint();
        vacuna4.setStyle(Paint.Style.FILL);
        vacuna4.setStrokeWidth(0);
        vacuna4.setColor(getResources().getColor(R.color.DTPRefuerzoColor));

        Paint vacuna5 = new Paint();
        vacuna5.setStyle(Paint.Style.FILL);
        vacuna5.setStrokeWidth(0);
        vacuna5.setColor(getResources().getColor(R.color.RotavirusColor));


        //Atras
        canvas.drawRect(new RectF(startX+50,alturaCentro-2120, lstartX-50,110), pintapinta1);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(startX+50,alturaCentro-2120, lstartX-50,60), 50, 50, pintapinta1);


        Paint pintaCentralVacunas = new Paint();
        pintaCentralVacunas.setStyle(Paint.Style.FILL);
        pintaCentralVacunas.setStrokeWidth(0);
        pintaCentralVacunas.setColor(getResources().getColor(R.color.tiposVacunas));


        //Atras
        canvas.drawRect(new RectF(startX+450,alturaCentro, lstartX-570,2200), pintapinta2);//Atras
        canvas.drawRect(new RectF(startX+450,alturaCentro-2120, lstartX-50,alturaCentro-2050), pintapinta2);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(startX+450,alturaCentro-2120, lstartX-50,alturaCentro), 50, 50, pintapinta2);

        //"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
        canvas.drawRect(new RectF(startX+50,alturaCentro-2120, startX+80,alturaCentro-1410), vacuna1);//Atras

        canvas.drawRect(new RectF(startX+50,alturaCentro-1410, startX+80,alturaCentro-1920), vacuna2);//Atras

        canvas.drawRect(new RectF(startX+50,alturaCentro-1410, startX+80,alturaCentro-830), vacuna3);//Atras

        canvas.drawRect(new RectF(startX+50,alturaCentro-830, startX+80,alturaCentro-450), vacuna4);//Atras


        //Atras
        canvas.drawRect(new RectF(startX+50,alturaCentro-450, startX+350,alturaCentro-100), vacuna5);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(startX+50,alturaCentro-450, startX+300,alturaCentro), 50, 50, vacuna5);

        canvas.drawRect(startX+80,alturaCentro-450, startX+450,alturaCentro, pintaCentralVacunas);


        //______________________________________
        dibujaLineasparteCentral(canvas);

    }


    //LINEAS CENTRALES --------------------------------------------------------------------------------------------
    private void dibujaLineasparteCentral(Canvas canvas){
        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int starXl = startX+50;

        Paint pintaCentra1 = new Paint();
        pintaCentra1.setStyle(Paint.Style.FILL);
        pintaCentra1.setStrokeWidth(3);
        pintaCentra1.setColor(getResources().getColor(R.color.lineasCompletasCentral));

        Paint pintaCentras = new Paint();
        pintaCentras.setStyle(Paint.Style.FILL);
        pintaCentras.setStrokeWidth(3);
        pintaCentras.setColor(getResources().getColor(R.color.lineasCompletasCentral));


        //Linea uno
        canvas.drawLine(starXl+400, 60, starXl+400, alturaCartilla-60, pintaCentras);

        //Linea dos
        canvas.drawLine(startX+650, 60, startX+650, alturaCartilla-60, pintaCentra1);

        //Linea tres
        canvas.drawLine(startX+900, 60, startX+900, alturaCartilla-60, pintaCentra1);


        //-- Lineas e informacion ------------------------------------
        lineasCaudriculasCentral(canvas);
        pintaInformacionVacunasParteCentral(canvas);

    }

    //LINEAS PARA CUADRICULAS CENTRALES ---------------------------------------------------------------------------
    private void lineasCaudriculasCentral(Canvas canvas){

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int lstartX = (anchoCartilla/3) * 2;


        Paint pintaCentra = new Paint();
        pintaCentra.setStyle(Paint.Style.FILL);
        pintaCentra.setStrokeWidth(4);
        pintaCentra.setColor(getResources().getColor(R.color.lineasCuadriculares));

        int alturaCentro = 2420;

        canvas.drawLine(startX+50, alturaCentro-2120, lstartX-50 , alturaCentro-2120, pintaCentra);

        canvas.drawLine(startX+50, alturaCentro-1920, lstartX-50 , alturaCentro-1920, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 2 LINEAS MAS
            canvas.drawLine(startX+450, alturaCentro-1750, lstartX-50 , alturaCentro-1750, pintaCentra);
            canvas.drawLine(startX+450, alturaCentro-1580, lstartX-50 , alturaCentro-1580, pintaCentra);

        canvas.drawLine(startX+50, alturaCentro-1410, lstartX-50 , alturaCentro-1410, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 3 LINEAS MAS
            canvas.drawLine(startX+450, alturaCentro-1265, lstartX-50 , alturaCentro-1265, pintaCentra);
            canvas.drawLine(startX+450, alturaCentro-1120, lstartX-50 , alturaCentro-1120, pintaCentra);
            canvas.drawLine(startX+450, alturaCentro-975, lstartX-50 , alturaCentro-975, pintaCentra);

        canvas.drawLine(startX+50, alturaCentro-830, lstartX-50 , alturaCentro-830, pintaCentra);
        //VACUNA ROTAVIRUS
        canvas.drawLine(startX+50, alturaCentro-450, lstartX-50 , alturaCentro-450, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 2 LINEAS MAS
            canvas.drawLine(startX+450, alturaCentro-150, lstartX-50 , alturaCentro-150, pintaCentra);
            canvas.drawLine(startX+450, alturaCentro-300, lstartX-50 , alturaCentro-300, pintaCentra);

    }

    //DIBUJA LA INFORMACION DE LAS VACUNAS-------------------------------------------------------------------------
    private void pintaInformacionVacunasParteCentral(Canvas canvas){
        Log.d("Log consola","muestra informacion las vacunas parte Central ");

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int starXl = startX+50;
        int alturaCentro = 2420;


        //-- MUESTRA INFO ENCABEZADO -----------------------------------------------------------------
        Paint textoEncabezado = new Paint();
        textoEncabezado.setTextSize(42);
        textoEncabezado.setAntiAlias(true);
        textoEncabezado.setColor(getResources().getColor(R.color.textoEncabezado));
        Typeface mediumEncabezado = ResourcesCompat.getFont(this,R.font.medium);
        textoEncabezado.setTypeface(mediumEncabezado);


        //----- MUESTRA INFO TIPO VACUNA -------------------------------------------------------------
        Paint tipoVacuna = new Paint();
        tipoVacuna.setTextSize(44);
        tipoVacuna.setAntiAlias(true);
        tipoVacuna.setColor(getResources().getColor(R.color.textoEncabezado));
        Typeface mediumVacuna = ResourcesCompat.getFont(this,R.font.medium);
        tipoVacuna.setTypeface(mediumVacuna);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA TEXTO GRIS------------
        Paint datosGrisVacunas = new Paint();
        datosGrisVacunas.setTextSize(36);
        datosGrisVacunas.setAntiAlias(true);
        datosGrisVacunas.setColor(getResources().getColor(R.color.textoGrisDatosVacunas));
        Typeface boldVacuna = ResourcesCompat.getFont(this,R.font.bold);
        datosGrisVacunas.setTypeface(boldVacuna);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA TEXTO NEGRO------------
        Paint datosNegroVacunas = new Paint();
        datosNegroVacunas.setTextSize(38);
        datosNegroVacunas.setAntiAlias(true);
        datosNegroVacunas.setColor(getResources().getColor(R.color.textoNegroDatosVacunas));
        Typeface boldVacunaNegrita = ResourcesCompat.getFont(this,R.font.bold);
        datosNegroVacunas.setTypeface(boldVacunaNegrita);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA MESES ------------
        Paint mesesVacunas = new Paint();
        mesesVacunas.setTextSize(39);
        mesesVacunas.setAntiAlias(true);
        mesesVacunas.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldVacunaVacunas = ResourcesCompat.getFont(this,R.font.regular);
        mesesVacunas.setTypeface(boldVacunaVacunas);

        //TEXTO ENCABEZADO------
        canvas.drawText(getString(R.string.vacunaTexto), starXl+132,190, textoEncabezado);
        canvas.drawText(getString(R.string.dosisTexto), starXl+447,190, textoEncabezado);

        canvas.drawText(getString(R.string.Edad1), starXl+648,150, textoEncabezado);
        canvas.drawText(getString(R.string.Edad2), starXl+630,190, textoEncabezado);
        canvas.drawText(getString(R.string.Edad3), starXl+655,235, textoEncabezado);

        canvas.drawText(getString(R.string.fechaT1), starXl+873,170, textoEncabezado);
        canvas.drawText(getString(R.string.fechaT12), starXl+863,210, textoEncabezado);


        //VACUNA BCG-------------
        canvas.drawText(getString(R.string.bcgTexto), startX+220,alturaCentro-2060, tipoVacuna);
        canvas.drawText(getString(R.string.bcgTexto1), startX+95,alturaCentro-2000, datosNegroVacunas);
        canvas.drawText(getString(R.string.bcgTexto2), startX+95,alturaCentro-1950, datosGrisVacunas);


        //VACUNA HEPATITIS B ---------
        canvas.drawText(getString(R.string.hepatitisTexto), startX+150,alturaCentro-1830, tipoVacuna);
        canvas.drawText(getString(R.string.hepatitisTexto1), startX+100,alturaCentro-1750, datosNegroVacunas);
        canvas.drawText(getString(R.string.hepatitisTexto2), startX+100,alturaCentro-1700, datosGrisVacunas);

        //
        canvas.drawText(getString(R.string.neumoText6), startX+95,alturaCentro-1600, datosNegroVacunas);
        canvas.drawText(getString(R.string.hepatitisTexto4), startX+95,alturaCentro-1550, datosGrisVacunas);

        //VACUNA PENTAVALENTE ACELULAR ---------
        canvas.drawText(getString(R.string.pentavalenteTexto1), startX+130,alturaCentro-1360, tipoVacuna);
        canvas.drawText(getString(R.string.pentavalenteTexto2), startX+180,alturaCentro-1320, tipoVacuna);

        canvas.drawText(getString(R.string.pentavalenteTexto3), startX+95,alturaCentro-1250, datosNegroVacunas);
        canvas.drawText(getString(R.string.pentavalenteTexto4), startX+95,alturaCentro-1200, datosGrisVacunas);
        canvas.drawText(getString(R.string.pentavalenteTexto5), startX+95,alturaCentro-1160, datosGrisVacunas);
        canvas.drawText(getString(R.string.pentavalenteTexto6), startX+95,alturaCentro-1120, datosGrisVacunas);
        canvas.drawText(getString(R.string.pentavalenteTexto7), startX+95,alturaCentro-1080, datosGrisVacunas);

                //----- MUESTRA INFO TIPO CADA VACUNA VACUNA TEXTO GRIS------------
        canvas.drawText(getString(R.string.pentavalenteTexto8), startX+95,alturaCentro-1040, datosGrisVacunas);

        canvas.drawText(getString(R.string.pentavalenteTexto9), startX+95,alturaCentro-1000, datosGrisVacunas);

        canvas.drawText(getString(R.string.neumoText6), startX+90,alturaCentro-920, datosNegroVacunas);
        canvas.drawText(getString(R.string.pentavalenteTexto11), startX+95,alturaCentro-870, datosGrisVacunas);

        //VACUNA DTP REFUERZO -------------
        canvas.drawText(getString(R.string.dtpTexto1), startX+130,alturaCentro-782, tipoVacuna);
        canvas.drawText(getString(R.string.dtpTexto2), startX+95,alturaCentro-726, datosNegroVacunas);
        canvas.drawText(getString(R.string.dtpTexto3), startX+95,alturaCentro-675, datosGrisVacunas);
        canvas.drawText(getString(R.string.dtpTexto4), startX+95,alturaCentro-645, datosGrisVacunas);
        canvas.drawText(getString(R.string.dtpTexto5), startX+95,alturaCentro-610, datosGrisVacunas);

        canvas.drawText(getString(R.string.neumoText6), startX+90,alturaCentro-545, datosNegroVacunas);

        canvas.drawText(getString(R.string.dtpTexto7), startX+95,alturaCentro-505, datosGrisVacunas);
        canvas.drawText(getString(R.string.dtpTexto8), startX+95,alturaCentro-465, datosGrisVacunas);


        //VACUNA ROTAVIRUS -------------
        canvas.drawText(getString(R.string.rotaText1), startX+140,alturaCentro-380, tipoVacuna);

        canvas.drawText(getString(R.string.rotaText2), startX+100,alturaCentro-310, datosNegroVacunas);
        canvas.drawText(getString(R.string.rotaText00), startX+100,alturaCentro-260, datosGrisVacunas);
        canvas.drawText(getString(R.string.rotaText01), startX+100,alturaCentro-220, datosGrisVacunas);


        canvas.drawText(getString(R.string.neumoText6), startX+90,alturaCentro-135, datosNegroVacunas);

        Paint datosGrisVacunasMenorM = new Paint();
        datosGrisVacunasMenorM.setTextSize(34);
        datosGrisVacunasMenorM.setAntiAlias(true);
        datosGrisVacunasMenorM.setColor(getResources().getColor(R.color.textoGrisDatosVacunas));
        Typeface boldVacunaHMo = ResourcesCompat.getFont(this,R.font.bold);
        datosGrisVacunasMenorM.setTypeface(boldVacunaHMo);

        canvas.drawText(getString(R.string.rotaText5), startX+90,alturaCentro-80, datosGrisVacunasMenorM);
        canvas.drawText(getString(R.string.rotaText6), startX+90,alturaCentro-30, datosGrisVacunasMenorM);


        //INFROMACION ESTATICA CENTRAL COLUMNA DOS ----------------------------------------------------------------
        canvas.drawText(getString(R.string.unicaTexto), startX+500,alturaCentro-2000, mesesVacunas);

        canvas.drawText(getString(R.string.primera), startX+520,alturaCentro-1820, mesesVacunas);
        canvas.drawText(getString(R.string.segunda), startX+520,alturaCentro-1650, mesesVacunas);
        canvas.drawText(getString(R.string.tercera), startX+520,alturaCentro-1480, mesesVacunas);

        canvas.drawText(getString(R.string.primera), startX+520,alturaCentro-1325, mesesVacunas);
        canvas.drawText(getString(R.string.segunda), startX+520,alturaCentro-1175, mesesVacunas);
        canvas.drawText(getString(R.string.tercera), startX+520,alturaCentro-1025, mesesVacunas);
        canvas.drawText(getString(R.string.cuarta), startX+520,alturaCentro-890, mesesVacunas);

        canvas.drawText(getString(R.string.refuerzoTexto), startX+470,alturaCentro-630, mesesVacunas);

        canvas.drawText(getString(R.string.primera), startX+520,alturaCentro-360, mesesVacunas);
        canvas.drawText(getString(R.string.segunda), startX+520,alturaCentro-210, mesesVacunas);
        canvas.drawText(getString(R.string.tercera), startX+520,alturaCentro-63, mesesVacunas);


        //INFROMACION ESTATICA CENTRAL COLUMNA TRES ---------------------------------------------------------------
        Paint recienNacido = new Paint();
        recienNacido.setTextSize(36);
        recienNacido.setAntiAlias(true);
        recienNacido.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldVacunaRN = ResourcesCompat.getFont(this,R.font.regular);
        recienNacido.setTypeface(boldVacunaRN);

        canvas.drawText(getString(R.string.recienNacidoTexto), startX+660,alturaCentro-2000, recienNacido);

        canvas.drawText(getString(R.string.recienNacidoTexto), startX+660,alturaCentro-1820, recienNacido);
        canvas.drawText(getString(R.string.DosmesTexto), startX+695,alturaCentro-1650, mesesVacunas);
        canvas.drawText(getString(R.string.SeismesTexto), startX+695,alturaCentro-1480, mesesVacunas);

        canvas.drawText(getString(R.string.DosmesTexto), startX+695,alturaCentro-1325, mesesVacunas);
        canvas.drawText(getString(R.string.CuatromesTexto), startX+695,alturaCentro-1175, mesesVacunas);
        canvas.drawText(getString(R.string.SeismesTexto), startX+695,alturaCentro-1025, mesesVacunas);
        canvas.drawText(getString(R.string.UnAnio6mesTexto), startX+655,alturaCentro-890, recienNacido);

        canvas.drawText(getString(R.string.CuatroAnioTexto), startX+712,alturaCentro-630, mesesVacunas);

        canvas.drawText(getString(R.string.DosmesTexto), startX+695,alturaCentro-360, mesesVacunas);
        canvas.drawText(getString(R.string.CuatromesTexto), startX+695,alturaCentro-210, mesesVacunas);
        canvas.drawText(getString(R.string.SeismesTexto), startX+695,alturaCentro-63, mesesVacunas);

        //_____________________________________________________________________
        //pintaCirtculos
        circulosVacunasColoresCentral(canvas);

        //INFROMACION FECHA DINAMICA CENTRAL COLUMNA CUATRO -------------------
        pintaFechasVacunasCentral(canvas);
    }

    //METODO PARA PINTAR CIRCULOS COLUMNA CENTRAL -----------------------------------------------------------------
    private void circulosVacunasColoresCentral(Canvas canvas) {

        int anchoCartilla = 3508;
        int startX = anchoCartilla/3;
        int alturaCentro = 2420;

        Paint vacuna2 = new Paint();
        vacuna2.setStyle(Paint.Style.FILL);
        vacuna2.setStrokeWidth(0);
        vacuna2.setColor(getResources().getColor(R.color.HepatitisBColor));

        Paint vacuna3 = new Paint();
        vacuna3.setStyle(Paint.Style.FILL);
        vacuna3.setStrokeWidth(0);
        vacuna3.setColor(getResources().getColor(R.color.PentavalenteAcelularColor));

        Paint vacuna4 = new Paint();
        vacuna4.setStyle(Paint.Style.FILL);
        vacuna4.setStrokeWidth(0);
        vacuna4.setColor(getResources().getColor(R.color.DTPRefuerzoColor));

        Paint vacuna5 = new Paint();
        vacuna5.setStyle(Paint.Style.FILL);
        vacuna5.setStrokeWidth(0);
        vacuna5.setColor(getResources().getColor(R.color.RotavirusColor));

        canvas.drawCircle( startX+420, alturaCentro-1565, 18, vacuna2);

        canvas.drawCircle( startX+420, alturaCentro-885, 18, vacuna3);

        canvas.drawCircle( startX+420, alturaCentro-525, 18, vacuna4);
        canvas.drawCircle( startX+420, alturaCentro-480, 18, vacuna4);

        canvas.drawCircle( startX+420, alturaCentro-95, 18, vacuna5);
        canvas.drawCircle( startX+420, alturaCentro-45, 18, vacuna5);

        //PINTA SOBRE CIRCULOS
        pintaSobreCirculosCentral(canvas);
    }

    //METODO PARA PINTAR NUMEROS CIRCULOS CENTRAL -----------------------------------------------------------------
    private void pintaSobreCirculosCentral(Canvas canvas) {

        int anchoCartilla = 3508;
        int alturaCentro = 2420;
        int startX = anchoCartilla/3;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));

        canvas.drawText(getString(R.string.unoNumero), startX+410,alturaCentro-512, tipoNumero);
        canvas.drawText(getString(R.string.dosNumero), startX+410,alturaCentro-468, tipoNumero);

        canvas.drawText(getString(R.string.unoNumero), startX+410,alturaCentro-84, tipoNumero);
        canvas.drawText(getString(R.string.dosNumero), startX+410,alturaCentro-34, tipoNumero);
    }


    //DIBUJA LAS FECHAS DE LAS VACUNAS-----------------------------------------------------------------------------
    private void dibujarParteDerecha(Canvas canvas) {
        Log.d("Lienzo","Parte Derecha");

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;

        Paint pintaDerecha = new Paint();
        pintaDerecha.setStyle(Paint.Style.FILL);
        pintaDerecha.setStrokeWidth(0);
        pintaDerecha.setColor(getResources().getColor(R.color.tiposVacunas));

        RectF parteDerecha = new RectF(lstartX+50,alturaCartilla-60, anchoCartilla-50,60);
        canvas.drawRoundRect(parteDerecha,50,50,pintaDerecha);

        //RECTANGULOS COLUMNA DERECHA ----------------------------------------------
        pintaRectanguloconBordesDerecha(canvas);

    }

    //METODO DE RECTANGULOS PARTE DERECHA -------------------------------------------------------------------------
    private void pintaRectanguloconBordesDerecha(Canvas canvas){

        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;

        Paint pintaCentra = new Paint();
        pintaCentra.setStyle(Paint.Style.FILL);
        pintaCentra.setStrokeWidth(0);
        pintaCentra.setColor(getResources().getColor(R.color.colorCartilla));

        Paint pintapintaA = new Paint();
        pintapintaA.setStyle(Paint.Style.FILL);
        pintapintaA.setStrokeWidth(0);
        pintapintaA.setColor(getResources().getColor(R.color.tiposVacunas));

        Paint pintapinta1 = new Paint();
        pintapinta1.setStyle(Paint.Style.FILL);
        pintapinta1.setStrokeWidth(0);
        pintapinta1.setColor(getResources().getColor(R.color.encabezado));

        Paint pintapinta2 = new Paint();
        pintapinta2.setStyle(Paint.Style.FILL);
        pintapinta2.setStrokeWidth(0);
        pintapinta2.setColor(getResources().getColor(R.color.colorInformacionVacunas));


        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        Paint vacuna7 = new Paint();
        vacuna7.setStyle(Paint.Style.FILL);
        vacuna7.setStrokeWidth(0);
        vacuna7.setColor(getResources().getColor(R.color.InfluenzaColor));

        Paint vacuna8 = new Paint();
        vacuna8.setStyle(Paint.Style.FILL);
        vacuna8.setStrokeWidth(0);
        vacuna8.setColor(getResources().getColor(R.color.SRPColor));

        Paint vacuna9 = new Paint();
        vacuna9.setStyle(Paint.Style.FILL);
        vacuna9.setStrokeWidth(0);
        vacuna9.setColor(getResources().getColor(R.color.AntipoliomieliticaOralColor));

        Paint vacuna10 = new Paint();
        vacuna10.setStyle(Paint.Style.FILL);
        vacuna10.setStrokeWidth(0);
        vacuna10.setColor(getResources().getColor(R.color.SRColor));


        int alturaCentro = 2420;
        int inioLimite = lstartX+50;

        //Atras
        canvas.drawRect(new RectF(inioLimite,alturaCentro-2120, anchoCartilla-50,110), pintapinta1);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(inioLimite,alturaCentro-2120, anchoCartilla-50,60), 50, 50, pintapinta1);


        //Atras
        canvas.drawRect(new RectF(inioLimite+400,alturaCentro, anchoCartilla-150,2200), pintapinta2);//Atras
        canvas.drawRect(new RectF(inioLimite+400,alturaCentro-2120, anchoCartilla-50,alturaCentro-2050), pintapinta2);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(inioLimite+400,alturaCentro-2120, anchoCartilla-50,alturaCentro), 50, 50, pintapinta2);

        //RECTANGULOS DE COLORES DE VACUNAS
        canvas.drawRect(new RectF(inioLimite,alturaCentro-2120, inioLimite+30,alturaCentro-1580), vacuna6);//Atras

        canvas.drawRect(new RectF(inioLimite,alturaCentro-1580, inioLimite+30,alturaCentro-1080), vacuna7);//Atras

        canvas.drawRect(new RectF(inioLimite,alturaCentro-1080, inioLimite+30,alturaCentro-680), vacuna8);//Atras

        canvas.drawRect(new RectF(inioLimite,alturaCentro-680, inioLimite+30,alturaCentro-340), vacuna9);//Atras

        //Atras
        canvas.drawRect(new RectF(inioLimite,alturaCentro-340, anchoCartilla-720,alturaCentro-100), vacuna10);//Atras
        //adelante
        canvas.drawRoundRect(new RectF(inioLimite,alturaCentro-340, anchoCartilla-720,alturaCentro), 50, 50, vacuna10);
        canvas.drawRect(inioLimite+30,alturaCentro-340, anchoCartilla-720,alturaCentro, pintapintaA);

        //----------------------------------------------------------------------------------
        //LINEAS COLUMNA DERECHA-------------------
        dibujaLineasparteDerecha(canvas);
    }

    //METODO PINTA LINEAS COLUMNA DERECHA -------------------------------------------------------------------------
    private void dibujaLineasparteDerecha(Canvas canvas){

        int alturaCartilla = 2480;
        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;


        Paint pintaCentra1 = new Paint();
        pintaCentra1.setStyle(Paint.Style.FILL);
        pintaCentra1.setStrokeWidth(3);
        pintaCentra1.setColor(getResources().getColor(R.color.lineasCompletasCentral));

        Paint pintaCentras = new Paint();
        pintaCentras.setStyle(Paint.Style.FILL);
        pintaCentras.setStrokeWidth(3);
        //pintaCentras.setColor(getResources().getColor(R.color.lineasCompletasCentral));
        pintaCentras.setColor(getResources().getColor(R.color.lineasCompletasCentral));


        //LINEAS SEPARACION -------
        //Linea uno
        canvas.drawLine(inioLimite+400, 60, inioLimite+400, alturaCartilla-60, pintaCentras);
        //Linea dos
        canvas.drawLine(inioLimite+600, 60, inioLimite+600, alturaCartilla-738, pintaCentras);
        //Linea tres
        canvas.drawLine(inioLimite+850, 60, inioLimite+850, alturaCartilla-60, pintaCentras);

        //-------------------------------------------------------------
        //LINEAS CUADRICULARES E INFORMACION DERECHA --------
        lineasCaudriculasDerecha(canvas);
        pintaInformacionVacunasParteDerecha(canvas);

        //_---------------------------------------------------------------------------------------------------
        //MUESTRA FECHAS DINAMICAS VACUNAS COLUMNA 3 -----------
        pintaFechasVacunasDerecha(canvas);
    }

    //METODO PARA PINTAR LINEAS CAUDRICULAS -----------------------------------------------------------------------
    private void lineasCaudriculasDerecha(Canvas canvas){

        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;
        int alturaCentro = 2420;

        Paint pintaCentra = new Paint();
        pintaCentra.setStyle(Paint.Style.FILL);
        pintaCentra.setStrokeWidth(4);
        pintaCentra.setColor(getResources().getColor(R.color.lineasCuadriculares));


        canvas.drawLine(inioLimite, alturaCentro-2120, anchoCartilla-50 , alturaCentro-2120, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 2 LINEAS MAS
            canvas.drawLine(inioLimite+400, alturaCentro-1940, anchoCartilla-50 , alturaCentro-1940, pintaCentra);
            canvas.drawLine(inioLimite+400, alturaCentro-1760, anchoCartilla-50 , alturaCentro-1760, pintaCentra);

        canvas.drawLine(inioLimite, alturaCentro-1580, anchoCartilla-50 , alturaCentro-1580, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 2 LINEAS MAS
            canvas.drawLine(inioLimite+400, alturaCentro-1412, anchoCartilla-50 , alturaCentro-1412, pintaCentra);
            canvas.drawLine(inioLimite+400, alturaCentro-1246, anchoCartilla-50 , alturaCentro-1246, pintaCentra);

        canvas.drawLine(inioLimite, alturaCentro-1080, anchoCartilla-50 , alturaCentro-1080, pintaCentra);
            //LINEAS VACUNA ROTAVIRUS 1 LINEAS MAS
            canvas.drawLine(inioLimite+400, alturaCentro-880, anchoCartilla-50 , alturaCentro-880, pintaCentra);

        canvas.drawLine(inioLimite, alturaCentro-680, anchoCartilla-50 , alturaCentro-680, pintaCentra);

        canvas.drawLine(inioLimite, alturaCentro-340, anchoCartilla-50 , alturaCentro-340, pintaCentra);

    }

    //METODO PARA PINTAR LA INFORMACIO COLUMNA DERECHA ------------------------------------------------------------
    private void pintaInformacionVacunasParteDerecha(Canvas canvas) {

        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;

        //-- MUESTRA INFO ENCABEZADO -----------------------------------------------------------------
        Paint textoEncabezado = new Paint();
        textoEncabezado.setTextSize(42);
        textoEncabezado.setAntiAlias(true);
        textoEncabezado.setColor(getResources().getColor(R.color.textoEncabezado));
        Typeface mediano = ResourcesCompat.getFont(this,R.font.medium);
        textoEncabezado.setTypeface(mediano);

        canvas.drawText(getString(R.string.vacunaTexto), inioLimite+130,190, textoEncabezado);
        canvas.drawText(getString(R.string.dosisTexto), inioLimite+445,190, textoEncabezado);

        canvas.drawText(getString(R.string.Edad1), inioLimite+648,150, textoEncabezado);
        canvas.drawText(getString(R.string.Edad2), inioLimite+630,190, textoEncabezado);
        canvas.drawText(getString(R.string.Edad3), inioLimite+655,235, textoEncabezado);

        canvas.drawText(getString(R.string.fechaT1), inioLimite+873,170, textoEncabezado);
        canvas.drawText(getString(R.string.fechaT12), inioLimite+863,210, textoEncabezado);


        //----- MUESTRA INFO TIPO VACUNA -------------------------------------------------------------
        Paint tipoVacuna = new Paint();
        tipoVacuna.setTextSize(44);
        tipoVacuna.setAntiAlias(true);
        tipoVacuna.setColor(getResources().getColor(R.color.textoEncabezado));
        Typeface boldVacunaHw = ResourcesCompat.getFont(this,R.font.medium);
        tipoVacuna.setTypeface(boldVacunaHw);


        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA TEXTO GRIS------------
        Paint datosGrisVacunas = new Paint();
        datosGrisVacunas.setTextSize(37);
        datosGrisVacunas.setAntiAlias(true);
        datosGrisVacunas.setColor(getResources().getColor(R.color.textoGrisDatosVacunas));
        Typeface bold = ResourcesCompat.getFont(this,R.font.bold);
        datosGrisVacunas.setTypeface(bold);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA TEXTO NEGRO------------
        Paint datosNegroVacunas = new Paint();
        datosNegroVacunas.setTextSize(39);
        datosNegroVacunas.setAntiAlias(true);
        datosNegroVacunas.setColor(getResources().getColor(R.color.textoNegroDatosVacunas));
        Typeface boldNegrita = ResourcesCompat.getFont(this,R.font.bold);
        datosNegroVacunas.setTypeface(boldNegrita);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA MESES ------------
        Paint mesesVacunas = new Paint();
        mesesVacunas.setTextSize(39);
        mesesVacunas.setAntiAlias(true);
        mesesVacunas.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldNegritamasW = ResourcesCompat.getFont(this,R.font.regular);
        mesesVacunas.setTypeface(boldNegritamasW);

        //----- MUESTRA INFO TIPO CADA VACUNA VACUNA MESES NUAL HASTA  ------------
        Paint mesesVacunasTextoMenor = new Paint();
        mesesVacunasTextoMenor.setTextSize(37);
        mesesVacunasTextoMenor.setAntiAlias(true);
        mesesVacunasTextoMenor.setColor(getResources().getColor(R.color.mesesVacunas));
        Typeface boldNegritamas = ResourcesCompat.getFont(this,R.font.regular);
        mesesVacunasTextoMenor.setTypeface(boldNegritamas);

        //----- MUESTRA INFO TIPO CADA VACUNA DINAMICO ------------
        Paint mesesVacunasdINAMICO = new Paint();
        mesesVacunasdINAMICO.setTextSize(40);
        mesesVacunasdINAMICO.setAntiAlias(true);
        mesesVacunasdINAMICO.setColor(getResources().getColor(R.color.textoEncabezado));

        int alturaCentro = 2420;

        //Vacuna Neumococica Conjugada --------------------------------------------------------------------------------
        canvas.drawText(getString(R.string.neumoText1), inioLimite+70,alturaCentro-2020, tipoVacuna);
        canvas.drawText(getString(R.string.neumoText2), inioLimite+100,alturaCentro-1970, tipoVacuna);

        canvas.drawText(getString(R.string.neumoText3), inioLimite+50,alturaCentro-1880, datosNegroVacunas);
        canvas.drawText(getString(R.string.neumoText4), inioLimite+50,alturaCentro-1840, datosGrisVacunas);
        canvas.drawText(getString(R.string.neumoText5), inioLimite+50,alturaCentro-1800, datosGrisVacunas);

        canvas.drawText(getString(R.string.neumoText6), inioLimite+45,alturaCentro-1720, datosNegroVacunas);
        canvas.drawText(getString(R.string.neumoText7), inioLimite+50,alturaCentro-1670, datosGrisVacunas);
        canvas.drawText(getString(R.string.neumoText8), inioLimite+50,alturaCentro-1630, datosGrisVacunas);


        //Vacuna Influenza --------------------------------------------------------------------------------------------
        canvas.drawText(getString(R.string.influenzaText1), inioLimite+120,alturaCentro-1470, tipoVacuna);

        canvas.drawText(getString(R.string.influenzaText2), inioLimite+50,alturaCentro-1370, datosNegroVacunas);
        canvas.drawText(getString(R.string.influenzaText3), inioLimite+50,alturaCentro-1320, datosGrisVacunas);

        canvas.drawText(getString(R.string.influenzaText4), inioLimite+45,alturaCentro-1200, datosNegroVacunas);
        canvas.drawText(getString(R.string.influenzaText5), inioLimite+50,alturaCentro-1150, datosGrisVacunas);


        //Vacuna SRP  -------------------------------------------------------------------------------------------------
        canvas.drawText(getString(R.string.srpText1), inioLimite+170,alturaCentro-1010, tipoVacuna);

        canvas.drawText(getString(R.string.srpText2), inioLimite+50,alturaCentro-940, datosNegroVacunas);
        canvas.drawText(getString(R.string.srpText3), inioLimite+50,alturaCentro-890, datosGrisVacunas);
        canvas.drawText(getString(R.string.srpText4), inioLimite+50,alturaCentro-850, datosGrisVacunas);

        canvas.drawText(getString(R.string.srpText5), inioLimite+45,alturaCentro-760, datosNegroVacunas);
        canvas.drawText(getString(R.string.srpText6), inioLimite+50,alturaCentro-715, datosGrisVacunas);


        //Vacuna ANTIPOLIOMIELITICA  -------------------------------------------------------------------------------------------------
        canvas.drawText(getString(R.string.antiPolio1), inioLimite+45,alturaCentro-600, tipoVacuna);
        canvas.drawText(getString(R.string.antiPolio2), inioLimite+100,alturaCentro-560, tipoVacuna);

        canvas.drawText(getString(R.string.antiPolio3), inioLimite+50,alturaCentro-450, datosNegroVacunas);
        canvas.drawText(getString(R.string.antiPolio4), inioLimite+50,alturaCentro-410, datosGrisVacunas);


        //Vacuna SR  -------------------------------------------------------------------------------------------------
        canvas.drawText(getString(R.string.srText1), inioLimite+180,alturaCentro-270, tipoVacuna);

        canvas.drawText(getString(R.string.srText2), inioLimite+50,alturaCentro-200, datosNegroVacunas);
        canvas.drawText(getString(R.string.srText3), inioLimite+50,alturaCentro-160, datosGrisVacunas);

        canvas.drawText(getString(R.string.srText4), inioLimite+45,alturaCentro-70, datosNegroVacunas);
        canvas.drawText(getString(R.string.srText5), inioLimite+50,alturaCentro-30, datosGrisVacunas);


        //INFORMACION MESES DE VACUNAS ---------------------------------------------------------------------------
        canvas.drawText(getString(R.string.primera), inioLimite+470,alturaCentro-2010, mesesVacunas);
        canvas.drawText(getString(R.string.segunda), inioLimite+470,alturaCentro-1830, mesesVacunas);
        canvas.drawText(getString(R.string.refuerzoTexto), inioLimite+420,alturaCentro-1660, mesesVacunas);

        canvas.drawText(getString(R.string.primera), inioLimite+470,alturaCentro-1480, mesesVacunas);
        canvas.drawText(getString(R.string.segunda), inioLimite+470,alturaCentro-1310, mesesVacunas);
        canvas.drawText(getString(R.string.adicionalText), inioLimite+415,alturaCentro-1150, mesesVacunas);

        canvas.drawText(getString(R.string.primera), inioLimite+470,alturaCentro-970, mesesVacunas);
        canvas.drawText(getString(R.string.refuerzoTexto), inioLimite+420,alturaCentro-770, mesesVacunas);

        //-_-_----____________-------------------------
        canvas.drawText(getString(R.string.adicionalText), inioLimite+555,alturaCentro-495, mesesVacunas);
        canvas.drawText(getString(R.string.adicionalText), inioLimite+555,alturaCentro-160, mesesVacunas);


        //Columna 3 de parte derecha----------------------------------------------------------------
        canvas.drawText(getString(R.string.DosmesTexto), inioLimite+645,alturaCentro-2010, mesesVacunas);
        canvas.drawText(getString(R.string.CuatromesTexto), inioLimite+645,alturaCentro-1830, mesesVacunas);
        canvas.drawText(getString(R.string.UnAnioTexto), inioLimite+670,alturaCentro-1660, mesesVacunas);

        canvas.drawText(getString(R.string.SeismesTexto), inioLimite+645,alturaCentro-1480, mesesVacunas);
        canvas.drawText(getString(R.string.SietemesTexto), inioLimite+645,alturaCentro-1310, mesesVacunas);
        canvas.drawText(getString(R.string.AnualText1), inioLimite+622,alturaCentro-1170, mesesVacunasTextoMenor);
        canvas.drawText(getString(R.string.AnualText2), inioLimite+613,alturaCentro-1130, mesesVacunasTextoMenor);

        canvas.drawText(getString(R.string.UnAnioTexto), inioLimite+670,alturaCentro-970, mesesVacunas);
        canvas.drawText(getString(R.string.SeisAnioTexto), inioLimite+670,alturaCentro-770, mesesVacunas);


        //_---------------------------------------------
        //MUESTRA CIRCULOS VACUNAS COLUMNA 1
        circulosVacunasColoresDerecha(canvas);
    }

    //METODO PARA PINTAR CIRCULOS COLUMNA DERECHA ---------------------------------------------
    private void circulosVacunasColoresDerecha(Canvas canvas) {

        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;
        int alturaCentro = 2420;

        Paint vacuna6 = new Paint();
        vacuna6.setStyle(Paint.Style.FILL);
        vacuna6.setStrokeWidth(0);
        vacuna6.setColor(getResources().getColor(R.color.NeumococicaColor));

        Paint vacuna7 = new Paint();
        vacuna7.setStyle(Paint.Style.FILL);
        vacuna7.setStrokeWidth(0);
        vacuna7.setColor(getResources().getColor(R.color.InfluenzaColor));

        Paint vacuna8 = new Paint();
        vacuna8.setStyle(Paint.Style.FILL);
        vacuna8.setStrokeWidth(0);
        vacuna8.setColor(getResources().getColor(R.color.SRPColor));

        Paint vacuna10 = new Paint();
        vacuna10.setStyle(Paint.Style.FILL);
        vacuna10.setStrokeWidth(0);
        vacuna10.setColor(getResources().getColor(R.color.SRColor));

        canvas.drawCircle( inioLimite +372, alturaCentro-1685, 18, vacuna6);
        canvas.drawCircle( inioLimite +372, alturaCentro-1642, 18, vacuna6);

        canvas.drawCircle( inioLimite +372, alturaCentro-1166, 18, vacuna7);
        canvas.drawCircle( inioLimite +372, alturaCentro-730, 18, vacuna8);
        canvas.drawCircle( inioLimite +372, alturaCentro-43, 18, vacuna10);

        //___________________________________________________________________
        //PINTA TEXTO SOBRE CIRCULOS DERECHA COLUMNA 1 -----------
        pintaSobreCirculosDerecha(canvas);
    }

    //METODO PARA PINTAR NUMEROS SOBRE CIRCULOS DEERECHA -----------------
    private void pintaSobreCirculosDerecha(Canvas canvas) {
        int anchoCartilla = 3508;
        int lstartX = (anchoCartilla/3) * 2;
        int inioLimite = lstartX+50;
        int alturaCentro = 2420;

        Paint tipoNumero = new Paint();
        tipoNumero.setTextSize(36);
        tipoNumero.setAntiAlias(true);
        tipoNumero.setColor(getResources().getColor(R.color.textoBlanco));

        canvas.drawText(getString(R.string.unoNumero), inioLimite +362,alturaCentro-1672, tipoNumero);
        canvas.drawText(getString(R.string.dosNumero), inioLimite +362,alturaCentro-1630, tipoNumero);
    }

    //Pinta imagnen PDF
    private void imagenFinal(Canvas canvas) {

        int alturaCartilla = 2480;
        int anchoCartilla = 3507;

        Drawable imagen = getDrawable(R.drawable.image_cartilla);

        imagen.setBounds(0,0,anchoCartilla,alturaCartilla);
        imagen.draw(canvas);

    }

}

